# Draggable Carousel - THT

Hi, thanks for visiting the repository. This project is created as a purpose of a take home coding exam.

It includes a reusable Draggable Carousel component as its main feature built in ReactJS and Typescript.

Hope you guys like it!👋

## Requirement:

- Node Version: v14.18.1
- VS Code or any other editors (i.e. Sublime Text or Jetbrains)

## Installation

- `yarn or npm install`

In the project directory, you can run:

### `yarn start` or `npm run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
