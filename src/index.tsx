import React from "react";
import ReactDOM from "react-dom";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import Home from "@routes/home/Home";
import "@assets/base.sass";

const App = () => {
	return (
		<div className="pretia-ui-html pretia-ui">
			<React.StrictMode>
				<BrowserRouter>
					<Routes>
						<Route path="/" element={<Home />} />
					</Routes>
				</BrowserRouter>
			</React.StrictMode>
		</div>
	);
};

ReactDOM.render(<App />, document.querySelector("#root"));
