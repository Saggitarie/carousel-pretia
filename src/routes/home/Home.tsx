import React, { useState } from "react";

import Carousel from "@components/carousal/Carousel";
import carouselItems from "@assets/data/carousel-items.json";

import "@routes/home/Home.sass";

const Home: React.FC = () => {
	const [isDragging, setIsDragging] = useState<boolean>(false);

	const toggleDraggingState = (draggingState: boolean) => {
		setIsDragging(draggingState);
	};
	return (
		<div id="home" className="container center space-items">
			<h1>Draggable Carousal</h1>
			<Carousel isDragging={toggleDraggingState}>
				{carouselItems.map((carouselItem, i) => {
					return (
						<a
							href={carouselItem.link}
							target="_blank"
							key={i}
							rel="noreferrer"
							className={isDragging ? "disable-links" : ""}
						>
							<img
								src={`${process.env.PUBLIC_URL}/images/${carouselItem.imageName}.jpg`}
								alt={`${carouselItem.imageName}`}
								className="fit-size"
							/>
						</a>
					);
				})}
			</Carousel>

			<Carousel isDragging={toggleDraggingState}>
				{carouselItems.map((carouselItem, i) => {
					return (
						<a
							href={carouselItem.link}
							target="_blank"
							key={i}
							rel="noreferrer"
							className={isDragging ? "disable-links" : ""}
						>
							<div className="center">
								<img
									src={`${process.env.PUBLIC_URL}/images/${carouselItem.imageName}.jpg`}
									alt={`${carouselItem.imageName}`}
									className="small"
								/>
								<h1>{carouselItem.title}</h1>
							</div>
						</a>
					);
				})}
			</Carousel>
		</div>
	);
};

export default Home;
