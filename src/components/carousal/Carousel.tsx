import React, { useState, useEffect, useRef } from "react";
import { ReactComponent as Arrow } from "@images/arrow.svg";

import "@components/carousal/Carousel.sass";

type isDraggingFunctionType = (draggingState: boolean) => void;

type Props = {
	isDragging: isDraggingFunctionType;
	children?: React.ReactNode;
};

const Carousel: React.FC<Props> = ({ isDragging, children }) => {
	const slides = useRef<HTMLDivElement>(null);
	const allSlides = useRef<HTMLDivElement>(null);
	const prevRef = useRef<HTMLButtonElement>(null);
	const nextRef = useRef<HTMLButtonElement>(null);

	const slideIndex = useRef<number>(0);
	const initialPosition = useRef<number>(0);
	const screenPositionX1 = useRef<number>(0);
	const screenPositionX2 = useRef<number>(0);
	const finalPosition = useRef<number | undefined>(0);

	const [slideWidth, setSlideWidth] = useState<number | undefined>(0);
	const [slideLength, setSlideLength] = useState<number | undefined>(0);

	useEffect(() => {
		setSlideWidth(allSlides.current?.offsetWidth);

		const allNodes = allSlides.current?.parentElement?.childNodes;

		const allNodesLength = allNodes?.length;

		setSlideLength(allNodesLength);

		const firstSlide = allNodes?.[0] as Node;
		const lastSlide = allNodes?.[allNodes?.length - 1] as Node;

		const duplicateFirstSlide = firstSlide.cloneNode(true);
		const duplicateLastSlide = lastSlide.cloneNode(true);

		slides.current?.appendChild(duplicateFirstSlide);
		slides.current?.insertBefore(duplicateLastSlide, firstSlide);
	}, []);

	const switchSlide = (slideDirection: string, slideMotion?: string) => {
		slides.current?.classList.add("transition");
		if (!slideMotion) {
			initialPosition.current = slides.current!.offsetLeft;
		}

		if (slideDirection === "next") {
			slides.current!.style.left = `${initialPosition.current - slideWidth!}px`;

			slideIndex.current++;
		} else {
			slides.current!.style.left = `${initialPosition.current + slideWidth!}px`;

			slideIndex.current--;
		}
	};

	const checkSlideIndex = () => {
		slides.current?.classList.remove("transition");
		if (slideIndex.current === -1) {
			slides.current!.style.left = `-${slideLength! * slideWidth!}px`;

			slideIndex.current = slideLength! - 1;
		}

		if (slideIndex.current === slideLength) {
			slides.current!.style.left = `-${1 * slideWidth!}px`;

			slideIndex.current = 0;
		}
	};

	const dragStart = (e: React.MouseEvent<HTMLDivElement>) => {
		e.preventDefault();

		initialPosition.current = slides.current!.offsetLeft;

		if (e.type === "mousedown" && e.nativeEvent instanceof MouseEvent) {
			screenPositionX1.current = e.nativeEvent.clientX;

			document.addEventListener("mousemove", dragMove);
			document.addEventListener("mouseup", dragEnd);
		}
	};

	const dragMove = (e: React.MouseEvent<HTMLElement> | MouseEvent) => {
		if (e.type === "mousemove" && e instanceof MouseEvent) {
			screenPositionX2.current = screenPositionX1.current - e.clientX;
			screenPositionX1.current = e.clientX;

			slides.current!.style.left = `${
				slides.current!.offsetLeft - screenPositionX2.current
			}px`;

			isDragging(true);
		}
	};

	const dragEnd = () => {
		/*
    Three Possibility of Dragging Movement:
    1. User Attempting to go to the next slide
    2. User Attempting to go to the previous slide
    3. User staying in the same position
    */
		finalPosition.current = slides.current?.offsetLeft;

		if (
			finalPosition.current! - initialPosition.current <
			-(slideLength! / 2)
		) {
			switchSlide("next", "dragging");
		} else if (
			finalPosition.current! - initialPosition.current >
			slideLength! / 2
		) {
			switchSlide("prev", "dragging");
		} else {
			slides.current!.style.left = `${initialPosition.current}px`;
		}

		document.removeEventListener("mousemove", dragMove);
		document.removeEventListener("mouseup", dragEnd);

		isDragging(false);
	};
	return (
		<div id="carousel" className="center">
			<div className="slider">
				<div
					className="slides flex"
					ref={slides}
					onTransitionEnd={checkSlideIndex}
					onMouseDown={dragStart}
				>
					{React.Children.map(children, (child, i) => {
						return (
							<div className="slide center" key={i} ref={allSlides}>
								{child}
							</div>
						);
					})}
				</div>
				<button
					className="prev-arrow"
					ref={prevRef}
					onClick={() => switchSlide("prev")}
				>
					<Arrow className="left-arrow" />
				</button>
				<button
					className="next-arrow"
					ref={nextRef}
					onClick={() => switchSlide("next")}
				>
					<Arrow className="right-arrow" />
				</button>
			</div>
		</div>
	);
};

export default Carousel;
